## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm run build`

Builds the app for production to the `build` folder.\

# Pensil Community Landing Page 🚀

This is a community landing page for community running in pensil platform.
This project has a well design user experience interface.
It has a setting page where Admin of the community can edit the look of the landing page.
Most importantly it is well responsive for all device type.

## Demo

demo : https://codercom.netlify.app/

## Design

| Desktop                     | Mobile                |
| :-------------------------- | :-------------------- |
| ![Desktop](screenshot1.png) | ![Mobile](mobile.png) |

|              Setting page              | Mobile Setting               |
| :------------------------------------: | :--------------------------- |
| ![Settingpage](screenshotsettings.png) | ![Mobile](mobilesetting.png) |

![Design and Development](screenshot2.png)

![Design and Development](screenshot3.png)

preview
![Design and Development](preview.png)
