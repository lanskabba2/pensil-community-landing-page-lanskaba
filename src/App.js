import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/home/Home";
import Settings from "./pages/settings/Settings";
import { useState, createContext } from "react";
import NotFound from "./pages/Not-found/NotFound";
import Groups from "./pages/groups/Groups";
import Bimage from "./images/banner.png";
export const AppContext = createContext();
function App() {
  const [isSidebarShow, setIsSidebarShow] = useState(false);
  const [isSearchShow, setIsSearchShow] = useState(false);
  const [bannerImage, setBannerImage] = useState(Bimage);
  const [companyName, setcompanyName] = useState("Coder Community");
  const [bannerText, setBannerText] = useState(
    "Learn From anywhere, Get help from here"
  );
  const [texthighlight, setTexthighlight] = useState(
    " Welcome to coder community. You might be learning from any place, company or resource, we are here to help you. Post any of your doubt and there are other fellow coders along with our team to solve your doubts."
  );
  const [desciptions, setDesciption] = useState([
    "We'll be there for you every step of the way, answering all your questions, solving any problem and ensuring that your journey is as smooth as possible.",
    "A community of people who are passionate about the same cause, dedicated to helping you better understand and implement their knowledge.",
    "Ask your questions without hesitation, knowing that someone will take the time to answer.",
    "connect to find likeminded people around the globe or close by for support and camaraderie as you take this journey together.",
  ]);
  const sidebarToggle = () => {
    setIsSidebarShow((prev) => !prev);
  };
  return (
    <div className="App">
      <AppContext.Provider
        value={{
          isSidebarShow,
          setIsSidebarShow,
          sidebarToggle,
          isSearchShow,
          setIsSearchShow,
          bannerImage,
          setBannerImage,
          companyName,
          setcompanyName,
          bannerText,
          setBannerText,
          texthighlight,
          setTexthighlight,
          desciptions,
          setDesciption,
        }}
      >
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="settings" element={<Settings />} />
            <Route path="groups" element={<Groups />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Router>
      </AppContext.Provider>
    </div>
  );
}

export default App;
