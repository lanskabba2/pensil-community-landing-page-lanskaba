import "./admins.css";
import { Link } from "react-router-dom";
import image from "../../images/avatar2.jpg";
import {
  FaInstagram,
  FaLinkedinIn,
  FaFacebookF,
  FaYoutube,
  FaTwitter,
} from "react-icons/fa";
function Admins() {
  return (
    <div className="admins__container">
      <h3 className="section__title">Owner/Instructor</h3>
      <div className="admin">
        <div className="row">
          <div className="admin__info">
            <div className="admin__image">
              <img src={image} alt="Owner of the community" />
            </div>
            <h3 className="admin__name">Lansana Kabba</h3>
            <span className="professor">Software Engineer</span>
          </div>
          <p className="admin__bio">
            I'm a Software Engineer with ability to learn and collaborate in
            rapidly changing environments and compositions. I like Programming
            and Coding. Most of the time I love to learn new technologies and
            programming languages. Coding is my passion. Currently I'm learning
            many different technologies in Web Design & Development.
          </p>
        </div>
        <div className="admin__social__links">
          <Link to="#" className="social__link">
            <FaInstagram />
          </Link>
          <Link to="#" className="social__link">
            <FaFacebookF />
          </Link>

          <Link to="#" className="social__link">
            <FaYoutube />
          </Link>

          <Link to="#" className="social__link">
            <FaLinkedinIn />
          </Link>
          <Link to="#" className="social__link">
            <FaTwitter />
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Admins;
