import "./banner.css";
import { Link } from "react-router-dom";
import { AppContext } from "../../App";
import { useContext } from "react";
//  =========Imports============
//  =========Imports============

function Banner() {
  const {
    bannerImage,
    setBannerImage,
    companyName,
    setcompanyName,
    bannerText,
    setBannerText,
  } = useContext(AppContext);
  // setBannerImage(Bimage);
  // setBannerText();
  return (
    <div className="banner__container">
      <div className="banner__overlay"></div>
      <img className="banner__image" src={bannerImage} alt="" />
      <div className="banner__content">
        <h2 className="banner__title">{companyName}</h2>
        <p className="banner__decs">{bannerText}</p>

        <Link className="btn btn-primary cta_btn" to="#">
          Join Now
        </Link>
      </div>
    </div>
  );
}

export default Banner;
