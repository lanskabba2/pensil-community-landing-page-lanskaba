import "./faqs.css";
import { FaPlus } from "react-icons/fa";
import { FaMinus } from "react-icons/fa";
import { useToggle } from "../../hooks/useToggle";
const Faq = ({ question, answer }) => {
  const [toggleFaq, setToggleFaq] = useToggle();
  return (
    <div className="faq" onClick={setToggleFaq}>
      <div className="question">
        <h4 className="question__title">{question}</h4>
        {toggleFaq ? <FaMinus className="icon" /> : <FaPlus className="icon" />}
      </div>
      {toggleFaq && <p className="answer">{answer}</p>}
    </div>
  );
};

export default Faq;
