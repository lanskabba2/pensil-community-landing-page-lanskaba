import "./faqs.css";
import Faq from "./Faq";
import { faqs } from "../../data";

const Faqs = () => {
  return (
    <div className="faqs__conatiner">
      <h3 className="section__title">FAQs</h3>
      <div className="faq_list">
        {faqs.map(({ id, question, answer }) => (
          <Faq key={id} question={question} answer={answer} />
        ))}
      </div>
    </div>
  );
};

export default Faqs;
