import "./groups.css";
import { Link } from "react-router-dom";
import { groups } from "../../data";
import { FaUsers } from "react-icons/fa";
const GroupCards = () => {
  return (
    <div className="group__list">
      {groups.map(({ name, path, image, desc }, index) => (
        <Link to={path} key={index} className="group">
          <div className="group_image">
            <img src={image} alt="Group Image" />
          </div>
          <div className="group__content">
            <h4 className="group__title">{name} </h4>
            <p className="group__desc">{desc.substr(0, 57)}..</p>
            <div className="meta">
              <div className="member">
                <FaUsers className="icon" />
                <span className="member__count">1275</span>
              </div>
              <Link className="join" to="/">
                Join
              </Link>
            </div>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default GroupCards;
