import "./groups.css";
import { Link } from "react-router-dom";
import GroupCards from "./GroupCards";
function Groups() {
  return (
    <div className="group__container ">
      <h3 className="section__title">Our Top Groups</h3>
      <GroupCards />
      <div className="view__all">
        <Link to="/groups" className="btn btn-primary">
          Explore All
        </Link>
      </div>
    </div>
  );
}

export default Groups;
