import "./listitem.css";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { AppContext } from "../../App";
import { useContext } from "react";
function Listitem() {
  const { desciptions } = useContext(AppContext);
  return (
    <div className="listitem ">
      <h2 className="section__title">Coder Community</h2>
      <div className="list__items">
        {desciptions.map((input, index) => (
          <div key={index} className="list__item ">
            <BsFillCheckCircleFill className="icon" />
            <p className="list__text">{input}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Listitem;
