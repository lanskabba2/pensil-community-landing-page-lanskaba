import "./navbar.css";
import Logo from "../../images/logo.png";
import { Link } from "react-router-dom";
// Icons import
import { GoThreeBars, GoSearch } from "react-icons/go";
import { GrClose } from "react-icons/gr";
import { HiOutlineUser } from "react-icons/hi";
// Icons import
import { AppContext } from "../../App";
import { useContext } from "react";
//  =========Imports============
//  =========Imports============
function Navbar() {
  const { isSidebarShow, isSearchShow, sidebarToggle, setIsSearchShow } =
    useContext(AppContext);
  return (
    <nav>
      <div className={`search_overlay ${isSearchShow ? "" : "active"}`}>
        <div className="search__input">
          <input type="search" placeholder="Search.." />
          <button onClick={() => setIsSearchShow(false)} className="search_btn">
            <GoSearch className="icon" />
          </button>
        </div>

        <div onClick={() => setIsSearchShow(false)} className="search__close">
          <GrClose className="icon" />
        </div>
      </div>

      <div className="nav__container">
        <div className="right">
          <div className="nav__toggle__btn" onClick={sidebarToggle}>
            {isSidebarShow ? (
              <GrClose className="icon" />
            ) : (
              <GoThreeBars className="icon" />
            )}
          </div>
          <Link className="logo" to="/">
            <img src={Logo} alt="Nav Logo" />
            <h1 className="logo__title">Coder Community</h1>
          </Link>
        </div>

        <div className="action__btns">
          <div
            onClick={() => setIsSearchShow(true)}
            className="nav__search__btn"
          >
            <GoSearch className="icon" />
          </div>
          <div className="nav__user__btn">
            <HiOutlineUser className="icon" />
          </div>
          <Link className="auth__btn btn btn-primary" to="#">
            <HiOutlineUser />
            <span>Login/Register</span>
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
