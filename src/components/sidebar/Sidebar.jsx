import "./sidebar.css";
import { groups, links } from "../../data";
import { NavLink } from "react-router-dom";
import { IoMdArrowDropright } from "react-icons/io";
import { AppContext } from "../../App";
import { useContext } from "react";
function Sidebar() {
  const { isSidebarShow, setIsSidebarShow } = useContext(AppContext);
  return (
    <div className={`sidebar ${isSidebarShow ? "sidebar__active" : ""}`}>
      <ul className="nav__links">
        {links.map(({ name, path, icon }, index) => (
          <li key={index} className="nav__links_item">
            <NavLink
              onClick={() => setIsSidebarShow(false)}
              className={({ isActive }) => (isActive ? "active__nav" : "")}
              to={path}
            >
              {icon}
              {name}
            </NavLink>
          </li>
        ))}
      </ul>
      <div className="divider"></div>
      <span className="side__title">Groups</span>
      <ul className="grouplist">
        {groups.map(({ name, path }, index) => (
          <li key={index} className="nav__links_item">
            <NavLink
              onClick={() => setIsSidebarShow(false)}
              className={({ isActive }) => (isActive ? "active__nav" : "")}
              to={path}
            >
              <IoMdArrowDropright className="icon" />
              {name.substr(0, 13)}....
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Sidebar;
