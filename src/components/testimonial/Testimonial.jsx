import "./testimonial.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";

import { ImQuotesLeft, ImQuotesRight } from "react-icons/im";
import { testimonials } from "../../data";
const Testimonial = () => {
  return (
    <div className="testimonial__container">
      <h3 className="section__title">
        <ImQuotesLeft className="icon" /> Testimonials
      </h3>

      <Swiper
        slidesPerView={"auto"}
        spaceBetween={20}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination]}
        className="mySwiper"
      >
        {testimonials.map(({ id, name, quote, avatar }) => (
          <SwiperSlide key={id}>
            <div className="testimonial">
              <div className="test__header">
                <div className="avatar">
                  <img src={avatar} alt="" />
                </div>
                <h3 className="test__name">{name}</h3>
              </div>
              <div className="test__body">
                <p>
                  <ImQuotesLeft className="qoute" />
                  {quote}
                  <ImQuotesRight className="qoute1" />
                </p>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default Testimonial;
