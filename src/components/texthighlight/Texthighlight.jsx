import "./texthighlight.css";
import { AppContext } from "../../App";
import { useContext } from "react";

function Texthighlight() {
  const { texthighlight } = useContext(AppContext);
  return (
    <div className="texthighlight">
      <p className="text">{texthighlight}</p>
    </div>
  );
}

export default Texthighlight;
