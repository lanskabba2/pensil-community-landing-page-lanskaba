import { IoHomeOutline } from "react-icons/io5";
import { IoNavigateCircleOutline } from "react-icons/io5";
import { IoCalendarOutline } from "react-icons/io5";
import { IoSettingsOutline } from "react-icons/io5";
export const links = [
  {
    name: "Home",
    path: "/",
    icon: <IoHomeOutline className="icon" />,
  },
  {
    name: "Explore Groups",
    path: "/groups",
    icon: <IoNavigateCircleOutline className="icon" />,
  },
  {
    name: "Events",
    path: "/event",
    icon: <IoCalendarOutline className="icon" />,
  },
  {
    name: "Settings",
    path: "/settings",
    icon: <IoSettingsOutline className="icon" />,
  },
];
export const groups = [
  {
    name: "Javascript Community",
    path: "/g/javascript-Community",
    image: require("./images/tmp-400-1655539036888.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "Python Community",
    path: "/g/python-Community",
    image: require("./images/tmp-428-1655558331489.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "Java Community",
    path: "/g/java-Community",
    image: require("./images/tmp-415-1655551106206.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "System Design Community",
    path: "/g/system-design-Community",
    image: require("./images/tmp-425-1655555608686.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "C/C++ Community",
    path: "/g/cpp-Community",
    image: require("./images/tmp-423-1655554295773.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "Mobile developer Community",
    path: "/g/mobile-Community",
    image: require("./images/tmp-416-1655551129136.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "front-End Community",
    path: "/g/front-end-Community",
    image: require("./images/tmp-427-1655558096008.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
  {
    name: "front-End Community",
    path: "/g/front-end-Community",
    image: require("./images/tmp-18-1655712979887.jpg"),
    desc: "Learn and help from each other. It doesn't matter from where you are learning, you can ask your doubts here. We all are here to help",
  },
];
export const faqs = [
  {
    id: 1,
    question: "Why should I opt for LKMedia?",
    answer:
      "LKMedia provides you the professional curated content by Indian instructors along with live doubt solving and personal one to one mentorship which you won't find anywhere else.",
  },
  {
    id: 2,
    question: "What is the validity of the courses and when can I watch them?",
    answer:
      "You will have lifetime access to the courses and can watch the lectures anytime you want. So it is totally flexible and provides you the comfort of learning anytime anywhere. Also as the technologies progress we keep on updating our courses so you get the access to them too.",
  },
  {
    id: 3,
    question:
      "How will my doubts be solved and will there be one on one interaction?",
    answer:
      "Your doubts will be solved on a live chat, as soon as you get a doubt just ping your mentor through the chat option and within 5-10 minutes you will be connected to him to solve your doubts.",
  },
  {
    id: 4,
    question:
      "Why don't you provide live classes and why should I prefer recorded lectures?",
    answer:
      "Tutedude doesn't believe in the idea of teaching 100 students in 1 class where the student sometimes feels hesitant to ask some doubts and where the other student feels that this student is wasting his time by asking silly doubts. Moreover in this busy world it becomes difficult to attend the classes on a specific schedule. So we combined the benefits and provide you interactive video lectures and live one on one doubt solving to learn at your own pace and comfort.",
  },
  {
    id: 5,
    question: "Will I get a certificate on Program completion?",
    answer:
      "Maiores fuga, cum praesentium esse laudantium! Distinctio nihil blanditiis accusantium atque, quo maxime inventore eum! Cum dolorem quibusdam amet et qui.",
  },
  {
    id: 6,
    question: " What are the system requirements to take this program?",
    answer:
      "Quas, est at! Molestiae quidem ab soluta exercitationem culpa nostrum iusto illum qui non a harum deserunt atque commodi at velit, consequatur quibusdam dignissimos cum labore possimus placeat consectetur nisi cupiditate.",
  },
];
export const testimonials = [
  {
    id: 1,
    name: "Diana Ayi",
    quote:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium ipsam facere ea a laboriosam sed? Quod vel voluptates a! Maxime minima cumque aut? In expedita numquam consectetur non officia iusto.",
    job: "Student",
    avatar: require("./images/avatar1.jpg"),
  },
  {
    id: 2,
    name: "Daniel Vinyo",
    quote:
      "Harum quaerat hic consequuntur molestias repellat ad quo tenetur vitae rem, labore quisquam? Atque, assumenda rerum this and that odit harum quaerat hic praesentium quisquam quae, enim iste ipsam id repellat.",
    job: "Software Egineer",
    avatar: require("./images/avatar2.jpg"),
  },
  {
    id: 3,
    name: "Edem Quist",
    quote:
      "Quaerat hic praesentium consequuntur molestias repellat ad quo tenetur vitae rem, labore quisquam? Atque, assumenda rerum odit harum quaerat hic praesentium quisquam quae, enim iste ipsam id repellat.",
    job: "University Lecturer",
    avatar: require("./images/avatar3.jpg"),
  },
  {
    id: 4,
    name: "Grace Lavoe",
    quote:
      "Cupiditate deleniti sint consequuntur molestias repellat ad quo tenetur vitae rem, labore quisquam? Atque, assumenda rerum odit harum quaerat hic praesentium quisquam quae, enim iste ipsam id repellat.",
    job: "Talking Parrot",
    avatar: require("./images/avatar4.jpg"),
  },
  {
    id: 5,
    name: "Nana Yaa Dankwa",
    quote:
      "Maxime minima cumque sit amet consectetur adipisicing elit. Praesentium ipsam facere ea a laboriosam sed? Quod vel voluptates a! Maxime minima cumque aut? In expedita numquam consectetur non officia iusto.",
    job: "Pharmacist",
    avatar: require("./images/avatar5.jpg"),
  },
];

const Admin1 = require("./images/avatar1.jpg");
const Admin2 = require("./images/avatar2.jpg");
const Admin3 = require("./images/avatar3.jpg");
const Admin4 = require("./images/avatar4.jpg");

export const admins = [
  {
    id: 1,
    image: Admin1,
    name: "Mary ",
    job: "Aerobic Admin",
    socials: [
      "https://instagram.com/",
      "https://twitter.com/",
      "https://facebook.com/",
      "https://linkedin.com/",
    ],
  },
  {
    id: 2,
    image: Admin2,
    name: "Daniel vinyo",
    job: "Speed Admin",
    socials: [
      "https://instagram.com/",
      "https://twitter.com/",
      "https://facebook.com/",
      "https://linkedin.com/",
    ],
  },
  {
    id: 3,
    image: Admin3,
    name: "Edem Quist",
    job: "Flexibility Admin",
    socials: [
      "https://instagram.com/",
      "https://twitter.com/",
      "https://facebook.com/",
      "https://linkedin.com/",
    ],
  },
  {
    id: 4,
    image: Admin4,
    name: "Shatta Wale",
    job: "Body Composition Admin",
    socials: [
      "https://instagram.com/",
      "https://twitter.com/",
      "https://facebook.com/",
      "https://linkedin.com/",
    ],
  },
];
