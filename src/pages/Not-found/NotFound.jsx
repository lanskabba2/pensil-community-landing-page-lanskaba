import { Link } from "react-router-dom";
import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sidebar/Sidebar";
function NotFound() {
  return (
    <>
      <Navbar />
      <div className="page__wrapper">
        <Sidebar />
        <div className="main__conainer">
          <div className="page__not__found">
            <h2>Page Not Found</h2>
            <Link to="/" className="btn btn-primary">
              Go Back Home
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default NotFound;
