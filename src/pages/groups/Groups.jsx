import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sidebar/Sidebar";
import GroupCards from "../../components/groups/GroupCards";
const Groups = () => {
  return (
    <>
      <Navbar />
      <div className="page__wrapper">
        <Sidebar />
        <div className="main__conainer">
          <br />
          <br />
          <h3 className="section__title">Community Groups</h3>
          <GroupCards />
        </div>
      </div>
    </>
  );
};

export default Groups;
