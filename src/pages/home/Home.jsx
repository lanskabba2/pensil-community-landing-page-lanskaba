import Navbar from "../../components/navbar/Navbar";
import Banner from "../../components/banner/Banner";
import Listitem from "../../components/listitem/Listitem";
import Texthighlight from "../../components/texthighlight/Texthighlight";
import Admins from "../../components/admin/Admins";
import Sidebar from "../../components/sidebar/Sidebar";
import Groups from "../../components/groups/Groups";
import Testimonial from "../../components/testimonial/Testimonial";
import Faqs from "../../components/faqs/Faqs";
function Home() {
  return (
    <>
      <Navbar />
      <div className="page__wrapper">
        <Sidebar />
        <div className="main__conainer">
          <Banner />
          <Texthighlight />
          <Listitem />
          <Admins />
          <Groups />
          <Testimonial />
          <Faqs />
        </div>
      </div>
    </>
  );
}

export default Home;
