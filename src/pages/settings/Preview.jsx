import "./settings.css";
import { IoClose } from "react-icons/io5";
import Banner from "../../components/banner/Banner";
import Listitem from "../../components/listitem/Listitem";
import Texthighlight from "../../components/texthighlight/Texthighlight";
import Admins from "../../components/admin/Admins";
import Groups from "../../components/groups/Groups";
import Testimonial from "../../components/testimonial/Testimonial";
import Faqs from "../../components/faqs/Faqs";

function Preview({ setpreview }) {
  return (
    <div className="preview__conatiner">
      <div className="preview_closebtn" onClick={() => setpreview(false)}>
        <IoClose className="icon" />
      </div>
      <div className="preview__landing">
        <Banner />
        <Texthighlight />
        <Listitem />
        <Admins />
        <Groups />
        <Testimonial />
        <Faqs />
      </div>
    </div>
  );
}

export default Preview;
