import "./settings.css";
import { Link } from "react-router-dom";
import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sidebar/Sidebar";
import Banner from "../../components/banner/Banner";
import Listitem from "../../components/listitem/Listitem";
import Texthighlight from "../../components/texthighlight/Texthighlight";
import { AiOutlineEye, AiOutlineHome } from "react-icons/ai";
import { MdPublic } from "react-icons/md";
import { useContext, useState } from "react";
import { AppContext } from "../../App";
import Preview from "./Preview";
function Settings() {
  const {
    bannerImage,
    setBannerImage,
    setcompanyName,
    setBannerText,
    setDesciption,
    setTexthighlight,
  } = useContext(AppContext);
  const getValue = (e) => {
    const textAreaValue = e.target.value;
    const textArr = textAreaValue.split("#");
    setDesciption(textArr);
  };
  const handleImage = (event) => {
    if (event.target.files.length > 0) {
      var sour = URL.createObjectURL(event.target.files[0]);
      console.log(sour);
      setBannerImage(sour);
    }
  };

  const [preview, setpreview] = useState(false);
  const cancelSetting = () => {
    // setBannerImage((prev) => !prev);
    // setcompanyName((prev) => !prev);
    // setBannerText,
    // setDesciption,
    // setTexthighlight,
  };
  return (
    <>
      <Navbar />
      <div className="page__wrapper setting__container">
        <Sidebar />
        <div className="main__conainer setting__page">
          <div className="header__edit">
            <div className="left">
              <Link to="/" className="btn setting" onClick={cancelSetting}>
                <span>Home</span>
                <AiOutlineHome className="icon" />
              </Link>
              <h2>Update Your Homepage</h2>
            </div>
            <div className="right">
              <button className="btn setting" onClick={() => setpreview(true)}>
                <span>Preview</span>
                <AiOutlineEye className="icon" />
              </button>

              <Link to="/" className="btn setting">
                <MdPublic className="icon" />
                <span>Publish</span>
              </Link>
            </div>
          </div>
          <div className="form__preview__container">
            <div className="form__container">
              <form>
                <div className="row top">
                  <div className="bimage">
                    <div className="image_upload">
                      <img
                        className="image__preview"
                        src={bannerImage}
                        alt=""
                      />
                      <input
                        onChange={handleImage}
                        type="file"
                        placeholder="Hero "
                        className="banner_image"
                      />
                    </div>
                    <label>Hero Image</label>
                  </div>

                  <div className="input">
                    <label htmlFor="cname">Hero Title:</label>
                    <input
                      type="text"
                      placeholder="Hero Title"
                      onChange={(e) => setcompanyName(e.target.value)}
                    />
                  </div>
                </div>

                <div className="row descript">
                  <div className="input__textarea">
                    <label>Banner Text:</label>
                    <textarea
                      placeholder="Banner text"
                      rows="4"
                      onChange={(e) => setBannerText(e.target.value)}
                    ></textarea>
                  </div>
                  <div className="input__textarea">
                    <label>Short Highlight Text:</label>
                    <textarea
                      placeholder="Short hightlight text"
                      rows="4"
                      onChange={(e) => setTexthighlight(e.target.value)}
                    ></textarea>
                  </div>
                  <div className="input__textarea">
                    <label>Community Description List:</label>
                    <span className="info">
                      Separate the List points by Hashtag symbol(#)
                    </span>
                    <textarea
                      onChange={getValue}
                      placeholder="Short hightlight text"
                      rows="5"
                    ></textarea>
                  </div>
                </div>
                <div className="space"></div>
              </form>
            </div>
            <div className="edit_preview">
              <Banner />
              <Texthighlight />
              <Listitem />
              <div className="space"></div>
            </div>
          </div>
          {preview && <Preview setpreview={setpreview} />}
        </div>
      </div>
    </>
  );
}

export default Settings;
